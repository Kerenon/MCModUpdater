#!/usr/bin/python3
import urllib.request
import requests
import os
import configparser
import sys

def downloadURL( settings, url ):
	"Downloads and saves into $path+filename.jar"
	response = requests.get(url)
	filename = response.history[-1].url.rsplit('/', 1)[-1]
	with open(settings['path'] + filename.replace("%20", " "), 'wb') as f:
		f.write(response.content)
	print("Downloaded: " + filename.replace("%20", " "))
	f = open('history', 'a')
	f.write(url +" "+filename.replace("%20", " ")+ "\n")
	f.close()
	return

def checkMod( settings, modname ):
	"Checks mod on CF"
	response = requests.get("https://minecraft.curseforge.com/projects/" + modname + "/files" + settings['version'])

	from bs4 import BeautifulSoup
	source_code = response.content
	soup = BeautifulSoup(source_code, "html.parser")
	inturl=""
	try:
		inturl="https://minecraft.curseforge.com" + soup.findAll('a', {'class': 'button tip fa-icon-download icon-only'})[0]['href']
		exists=urlExists(inturl)
		if exists == "yes":
			print("Mod version is up to date: " + modname)
		else:
			print("Found newer version of mod: " + modname )
			deleteOldMod(settings, modname)
			downloadURL(settings, inturl)
	except:
		print("Mod is unavailable to download for the version you selected(or some other error happened): " + modname)
	return

def urlExists( url ):
	with open("history", "r") as ins:
		for line in ins:
			if url in line.rstrip('\n'):
				return "yes"
def deleteOldMod( settings, modname ):
	lastline=""
	with open("history", "r") as ins:
		for line in ins:
			if "/"+modname+"/" in line.rstrip('\n'):
				lastline = line
	try:
		lastline=lastline.split(maxsplit=1)[1]
		print("Removing mod: " + settings['path']+lastline)
		os.remove(settings['path']+lastline)
	except Exception:
		pass
		#print("Exception in deleteOldMod function or mod is new")
	return

def loadSettings( settings ):
	settingsFileName='settings.cfg'
	defaultPath='/home/c0rn3j/Dropbox/Minecraft/Minecraft 1.11.2/mods/'
	defaultVersion='?filter-game-version=2020709689%3A6452'
	config = configparser.RawConfigParser()
	if os.path.exists(settingsFileName):
		config.read(settingsFileName)
		settings['path'] = config.get('settings', 'path')
		if not(settings['path']):
			settings['path']=defaultPath
			print('Configuration file incomplete, using default for "path": ' + settings['path'])
		settings['version'] = config.get('settings', 'version')
		if not(settings['version']):
			settings['version']=defaultVersion
			print('Configuration file incomplete, using default for "version": ' + settings['version'])
	else:
		config['settings']={'path': defaultPath,
		                    'version': defaultVersion }
		with open(settingsFileName, 'w') as configfile:
			config.write(configfile)
		print("The configuration file is missing, a new blank one has been created. Exiting.")
		sys.exit(1)
	return

def main ():
	settings={}
	loadSettings(settings)
	#In case history does not exist
	f = open('history', 'a')
	f.close()

	with open("modlist", "r") as ins:
		for line in ins:
			checkMod(settings, line.rstrip('\n'))

if __name__ == '__main__':
    main()
