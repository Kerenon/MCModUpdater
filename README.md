![alt tag](images/example.png)

Hey, this is just a simple python script that downloads all mods you specify for the version you specify.

Tested on GNU/Linux and [Windows](https://www.reddit.com/r/feedthebeast/comments/5zlzs7/i_was_pissed_with_total_lack_of_any_automatic_mod/dezggv8/).

I created this after I was pissed with manual checking of updates and manual updating of every single mod in my 180~ mod modpack.

The proprietary Curse client has the same functionality and more, but it is not available on Linux and this is a free and open source implementation. 

[There's a similar project to mine you may want to check out](https://github.com/Nincraft/ModPackDownloader)

---

**You'll need to install following libraries like so:**

pip install beautifulsoup4

pip install requests

---

**Usage:**

A mod name is taken from CF URL, so for Enchanting plus, the URL is https://minecraft.curseforge.com/projects/enchanting-plus which means the modname for the text file would be "enchanting-plus" without quotes.

Specify mod names in the file "modlist" separated by newlines(or just use the default modlist).

Edit the variable "path" in main.py to specify your own path for the mods. 

Edit the variable "version" with a filter of your own - go to CF, filter by version and take only the ?filter.. part and put it to the variable. Default is for version 1.11.2.

Example of the complete version URL - https://minecraft.curseforge.com/projects/abyssalcraft/files?filter-game-version=2020709689%3A6452

Then just run as following:

*python main.py*

History of downloaded files is available in a file called history - it's also the file that's used to compare whether there is a newer version or not.

---

**Known Issues:**

The script always downloads the newest file, so it may end up downloading an alpha release instead of a stable version. I don't really care about that, I want the alpha releases, but if you mind, feel free to submit a pull request that lets people choose.

---


If I saved you some time, you can send a coffee my way by PayPal spleefer90@gmail.com
